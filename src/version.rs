use crate::number::Number;
use crate::stage::Stage;

#[derive(PartialEq, Debug)]
pub struct Version {
    pub number: Number,
    pub stage: Stage,
}

impl Version {
    #[deprecated(since = "0.5.0", note = "superseded by `from`")]
    pub fn new(input: &str) -> Self {
        Self::from(input)
    }
}

impl From<&str> for Version {
    fn from(input: &str) -> Self {
        Self {
            number: Number::from(input),
            stage: Stage::from(input),
        }
    }
}

#[test]
fn eq() {
    let version_1 = Version::from("1.2.3-Alpha");
    let version_2 = Version::from("1.2.3-Alpha");
    assert_eq!(version_1, version_2);
}
