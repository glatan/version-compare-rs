/*!
A Rust library for comparing version. It supports some versioning styles(not only for X.Y.Z).

## Example

```rust
use vercomp::{Stage, Version};

// Version.number is the number of the version. It supports "==", "!=", ">=", ">", "<=" and "<".
let version_1 = Version::from("1.2.3");
let version_2 = Version::from("1-2-3");
if version_1.number == version_2.number {
    println!("version_1 equals version_2.");
}
let version_3 = Version::from("1/2/3");
let version_4 = Version::from("1~2");
if version_3.number > version_4.number {
    println!("version_3 is greater than version_4.");
}
let version_5 = Version::from("1:2");
let version_6 = Version::from("1;2;3");
if version_5.number < version_6.number {
    println!("version_5 is less than version_6.");
}

// Version.stage is the stage of the version. It only supports "==" and "!=".
let version_7 = Version::from("1.2.3-Alpha");
if version_7.stage == Stage::Alpha {
    println!("version_7 is an alpha version.");
}
let _version_8 = Version::from("1.2.3-Beta");
let _version_9 = Version::from("1.2.3");
// if _version_8.stage > _version_9.stage {
    // Compile error
    // Version.stage can not use ">=", ">", "<=" and "<".
// }
// "CASE INSENSITIVE"
// Stage::Alpha   <= Contain alpha
// Stage::Beta    <= Contain beta
// Stage::Dev     <= Contain dev
// Stage::Nightly <= Contain nightly
// Stage::Rc      <= Contain rc
// Stage::Stable  <= others
```
*/
mod number;
mod stage;
mod version;

pub use crate::stage::Stage;
pub use crate::version::Version;
