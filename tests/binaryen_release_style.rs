use vercomp::{Stage, Version};

use std::cmp::Ordering;

// Name: Binaryen
// Repository: https://github.com/WebAssembly/binaryen

#[test]
fn eq() {
    let version_1 = Version::from("version_90");
    let version_2 = Version::from("version_90");
    assert_eq!(version_1.number.cmp(&version_2.number), Ordering::Equal);
}

#[test]
fn gt() {
    let version_1 = Version::from("version_90");
    let version_2 = Version::from("version_89");
    assert_eq!(version_1.number.cmp(&version_2.number), Ordering::Greater);
}

#[test]
fn lt() {
    let version_1 = Version::from("version_89");
    let version_2 = Version::from("version_90");
    assert_eq!(version_1.number.cmp(&version_2.number), Ordering::Less);
}

#[test]
fn stable() {
    let version = Version::from("version_90");
    assert_eq!(version.stage, Stage::Stable);
}
