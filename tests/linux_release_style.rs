use vercomp::{Stage, Version};

use std::cmp::Ordering;

// Name: Linux
// Repository: https://github.com/torvalds/linux

#[test]
fn eq() {
    let version_1 = Version::from("v5.5-rc7");
    let version_2 = Version::from("v5.5-rc7");
    assert_eq!(version_1.number.cmp(&version_2.number), Ordering::Equal);
    assert_eq!(version_1.stage, version_2.stage);
}

#[test]
fn gt() {
    let version_1 = Version::from("v5.5-rc7");
    let version_2 = Version::from("v5.5-rc6");
    assert_eq!(version_1.number.cmp(&version_2.number), Ordering::Greater);
}

#[test]
fn lt() {
    let version_1 = Version::from("v5.5-rc6");
    let version_2 = Version::from("v5.5-rc7");
    assert_eq!(version_1.number.cmp(&version_2.number), Ordering::Less);
}

#[test]
fn stable() {
    let version = Version::from("v5.5");
    assert_eq!(version.stage, Stage::Stable);
}

#[test]
fn rc() {
    let version = Version::from("v5.6-rc2");
    assert_eq!(version.stage, Stage::RC);
}
